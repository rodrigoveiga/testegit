<?php
include 'classes/tarefa.php';

$codigo = 0;
$descricao = '';
$entrega = '';
$mensagem = '';
$status = 0;

if (!empty($_POST)) {
    if (isset($_POST['btn_status'])) {
        $mensagem = '<div class="alert alert-danger" role="alert">Erro ao alterar o status da tarefa!</div>';
        $tarefa = tarefa::getTarefa($_POST['codigo']);
        if ($tarefa && $tarefa->changeStatus()) {
            $mensagem = '<div class="alert alert-success" role="alert">Tarefa ' . ($tarefa->getStatus() ? 'concluída' : 'reaberta') . ' com sucesso!</div>';
        }
    } elseif (isset($_POST['btn_excluir'])) {
        $mensagem = '<div class="alert alert-danger" role="alert">Erro ao excluir a tarefa!</div>';
        $tarefa = tarefa::getTarefa($_POST['codigo']);
        if ($tarefa && $tarefa->delete()) {
            $mensagem = '<div class="alert alert-success" role="alert">Tarefa excluída com sucesso!</div>';
        }
    } elseif (isset($_POST['btn_enviar'])) {
        $tarefa = new tarefa($_POST['codigo'], $_POST['descricao'], $_POST['entrega'], $_POST['status']);
        $mensagem = '<div class="alert alert-danger" role="alert">Erro ao ' . ($_POST['codigo'] ? 'alterar' : 'cadastrar') . ' a tarefa!</div>';
        if ($tarefa->save()) {
            $mensagem = '<div class="alert alert-success" role="alert">Tarefa ' . ($_POST['codigo'] ? 'alterada' : 'cadastrada') . ' com sucesso!</div>';
        }
    } elseif (isset($_POST['btn_editar'])) {
        $codigo = $_POST['codigo'];
        $tarefa = tarefa::getTarefa($codigo);
        $descricao = $tarefa->getDescricao();
        $entrega = $tarefa->getDataEntrega();
        $status = $tarefa->getStatus();
    }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>To do list</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="style.css" type="text/css" media="screen, projection">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="container py-5">
            <div class="row">
                <div class="col-12 mb-5">
                    <h1 class="mb-5">To Do List</h1>
                    <?php echo $mensagem; ?>
                    <form method="POST" action="">
                        <div class="form-row align-items-end">
                            <input type="hidden" name="codigo" id="codigo" value="<?php echo $codigo; ?>">
                            <input type="hidden" name="status" id="status" value="<?php echo $status; ?>">
                            <div class="form-group col-12 col-md-8 col-lg-6">
                                <label for="descricao">Descrição</label>
                                <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $descricao; ?>" required autofocus>
                            </div>
                            <div class="form-group col-12 col-md-4 col-lg-3">
                                <label for="entrega">Data para Entrega</label>
                                <input type="date" name="entrega" id="entrega" class="form-control" value="<?php echo $entrega; ?>" required>
                            </div>
                            <div class="form-group col-12 col-md-3 ml-md-auto col-lg-3 ml-lg-0">
                                <button type="submit" name="btn_enviar" id="btn_enviar" class="btn btn-primary w-100" value="enviar"><?php echo $codigo ? 'Editar' : 'Cadastrar' ?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="thead-light">
                                <tr>
                                    <th>Tarefa</th>
                                    <th>Data para Entrega</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $tarefas = tarefa::listTarefas();

                                foreach ($tarefas as $tarefa) {
                                    ?>
                                    <tr>
                                        <td><?php echo $tarefa->getDescricao(); ?></td>
                                        <td><?php echo date('d/m/Y', strtotime($tarefa->getDataEntrega())); ?></td>
                                        <td>
                                            <form method="POST" action="">
                                                <input type="hidden" name="codigo" id="codigo" value="<?php echo $tarefa->getId(); ?>">
                                                <button type="submit" name="btn_status" class="btn <?php echo $tarefa->getStatus() ? 'btn-primary' : 'btn-success'; ?> btn-sm" value="status"><?php echo $tarefa->getStatus() ? 'Reabrir' : 'Concluir'; ?></button>
                                            </form>
                                        </td>
                                        <td>
                                            <form method="POST" action="">
                                                <input type="hidden" name="codigo" id="codigo" value="<?php echo $tarefa->getId(); ?>">
                                                <button type="submit" name="btn_editar" class="btn btn-primary btn-sm" value="editar">Editar</button>
                                            </form>
                                        </td>
                                        <td>
                                            <form method="POST" action="" onsubmit="return confirmation('<?php echo $tarefa->getDescricao(); ?>')">
                                                <input type="hidden" name="codigo" id="codigo" value="<?php echo $tarefa->getId(); ?>">
                                                <button type="submit" name="btn_excluir" class="btn btn-danger btn-sm" value="excluir">Excluir</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script>
            function confirmation(txt) {
                return confirm('Tem certeza que deseja excluir a tarefa "' + txt + '"?');
            }
        </script>
    </body>
</html>
