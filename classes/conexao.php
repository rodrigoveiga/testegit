<?php

class conexao {

    private $host = "localhost";
    private $user = "root";
    private $password = "";
    private $database = "futfanatics_tarefas";

    function __construct() {
    }

    private function connect() {
        $con = mysqli_connect($this->host, $this->user, $this->password, $this->database) or die("Erro ao conectar ao servidor &raquo; " . mysql_error());
        return $con;
    }

    static function executeQuery($query) {
        $banco = new conexao();
        $con = $banco->connect();
        if ($result = mysqli_query($con, $query)) {
            $banco->disconnect($con);
            return $result;
        } else {
            echo "Ocorreu um erro na execução da SQL";
            echo "Erro :" . mysqli_error($con);
            echo "SQL: " . $query;
            $banco->disconnect($con);
        }
        return false;
    }

    private function disconnect($con) {
        return mysqli_close($con);
    }

}

?>
