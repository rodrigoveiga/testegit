<?php

include "classes/conexao.php";

class tarefa {

    private $id;
    private $descricao;
    private $dataEntrega;
    private $status;

    function __construct($id = 0, $descricao = "", $dataEntrega = "", $status = 0) {
        $this->id = $id;
        $this->descricao = $descricao;
        $this->dataEntrega = $dataEntrega;
        $this->status = $status;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getDataEntrega() {
        return $this->dataEntrega;
    }

    public function setDataEntrega($dataEntrega) {
        $this->dataEntrega = $dataEntrega;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function save() {
        if ($this->getId()) {
            $tarefa = conexao::executeQuery('update tarefas set tar_descricao = "' . $this->descricao . '", tar_dataEntrega = "' . $this->dataEntrega . '", tar_status = ' . $this->status . ' where tar_id = ' . $this->id);
        } else {
            $tarefa = conexao::executeQuery('insert into tarefas (tar_descricao, tar_dataEntrega, tar_status) values ("' . $this->descricao . '", "' . $this->dataEntrega . '", 0)');
        }
        return $tarefa !== false ? $tarefa : false;
    }

    static function getTarefa($id) {
        $tarefa = conexao::executeQuery('select * from tarefas where tar_id = ' . $id);
        if ($tarefa->num_rows) {
            $tarefa = mysqli_fetch_assoc($tarefa);
            return new tarefa($tarefa["tar_id"], $tarefa["tar_descricao"], $tarefa["tar_dataEntrega"], $tarefa["tar_status"]);
        }
        return false;
    }

    static function listTarefas() {
        $result = conexao::executeQuery('select * from tarefas order by tar_status asc, tar_dataEntrega asc');
        $tarefas = array();
        while ($tarefa = mysqli_fetch_assoc($result)) {
            $tarefas[] = new tarefa($tarefa["tar_id"], $tarefa["tar_descricao"], $tarefa["tar_dataEntrega"], $tarefa["tar_status"]);
        }
        return $tarefas;
    }

    public function delete() {
        if ($this->id) {
            $tarefa = conexao::executeQuery('delete from tarefas where tar_id = ' . $this->id);
            return $tarefa ? true : false;
        }
        return false;
    }

    public function changeStatus() {
        if ($this->id) {
            $tarefa = conexao::executeQuery('update tarefas set tar_status = ' . ($this->status ? 0 : 1) . ' where tar_id = ' . $this->id);
            if ($tarefa) {
                $this->status = $this->status ? 0 : 1;
                return true;
            }
        }
        return false;
    }

}

?>
