-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Mai 12, 2019 as 08:15 PM
-- Versão do Servidor: 5.5.10
-- Versão do PHP: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `futfanatics_tarefas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefas`
--

CREATE TABLE IF NOT EXISTS `tarefas` (
  `tar_id` int(11) NOT NULL AUTO_INCREMENT,
  `tar_descricao` text NOT NULL,
  `tar_dataEntrega` date NOT NULL,
  `tar_status` int(1) NOT NULL,
  PRIMARY KEY (`tar_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tarefas`
--

INSERT INTO `tarefas` (`tar_id`, `tar_descricao`, `tar_dataEntrega`, `tar_status`) VALUES
(1, 'Tarefa 1', '2019-05-31', 0),
(3, 'Tarefa 3', '2019-05-30', 1);
